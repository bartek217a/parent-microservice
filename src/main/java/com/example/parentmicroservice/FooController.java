package com.example.parentmicroservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class FooController {

    @Autowired
    private FooProperties properties;

    @Autowired
    private BarEndpointClient barEndpointClient;

    @Autowired
    private MikeEndpointClient mikeEndpointClient;

    @GetMapping("/foo")
    public String foo(@RequestParam String query) {
        return String.format(
                "foo, id: %s, name: %s, bar endpoint response: %s, mike endpoint response: %s",
                properties.getId(),
                properties.getName(),
                barEndpointClient.perform(query),
                mikeEndpointClient.perform(query)
        );
    }

}
