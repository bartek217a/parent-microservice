package com.example.parentmicroservice;

import feign.RetryableException;
import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClientRetryerConfig {

    @Bean
    public Retryer retryer() {
        return new CustomRetryer();
    }

    private static class CustomRetryer implements Retryer {

        private static final int MAX_ATTEMPTS = 3;

        int attempt;

        CustomRetryer() {
            attempt = 1;
        }

        CustomRetryer(int attempt) {
            this.attempt = attempt;
        }

        @Override
        public void continueOrPropagate(RetryableException e) {
            if (attempt < MAX_ATTEMPTS) {
                attempt++;
            } else {
                throw e;
            }
        }

        @Override
        public Retryer clone() {
            return new CustomRetryer(attempt);
        }

    }

}
