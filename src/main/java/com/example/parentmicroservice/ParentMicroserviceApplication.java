package com.example.parentmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ParentMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParentMicroserviceApplication.class, args);
    }

}
