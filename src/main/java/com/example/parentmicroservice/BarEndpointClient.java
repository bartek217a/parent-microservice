package com.example.parentmicroservice;

import feign.FeignException;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        url = "${child-microservice.url}",
        name = "bar-endpoint-client",
        fallbackFactory = BarEndpointClient.BarEndpointClientFallbackFactory.class
)
public interface BarEndpointClient {

    @RequestMapping(value = "/bar")
    String perform(@RequestParam String query);

    @Component
    class BarEndpointClientFallbackFactory implements FallbackFactory<BarEndpointClient> {
        @Override
        public BarEndpointClient create(Throwable cause) {
            if (cause instanceof FeignException exception) {
                return query -> String.format("ERROR %s", exception.status());
            } else {
                return query -> "UNKNOWN ERROR";
            }
        }
    }

}
