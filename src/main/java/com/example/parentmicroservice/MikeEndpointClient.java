package com.example.parentmicroservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        url = "${child-microservice.url}",
        name = "mike-endpoint-client",
        fallback = MikeEndpointClient.MikeEndpointClientFallback.class
)
public interface MikeEndpointClient {

    @RequestMapping(value = "/mike")
    String perform(@RequestParam String query);

    @Component
    class MikeEndpointClientFallback implements MikeEndpointClient {

        @Override
        public String perform(String query) {
            System.out.printf("Mike endpoint query: %s%n", query);
            return "ERROR";
        }

    }

}
