FROM openjdk:18-jdk-alpine
COPY target/*.jar parent-microservice.jar
ENTRYPOINT ["java", "-jar", "parent-microservice.jar"]
