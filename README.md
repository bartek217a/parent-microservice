# Spring Cloud Config
- Refresh config by calling HTTP POST localhost:8080/actuator/refresh
- Beans must be annotated with @RefreshScope
